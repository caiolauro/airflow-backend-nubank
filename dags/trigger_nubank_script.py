
from datetime import datetime, timedelta
from airflow import DAG
from airflow_dbt_python.operators.dbt import DbtRunOperator
from airflow.operators.bash import BashOperator 

with DAG(
    'nubank_account_data_refresh',
    default_args={
        'depends_on_past': False,
        'email': ['airflow@example.com'],
        'email_on_failure': False,
        'email_on_retry': False,
        'retries': 1,
        'retry_delay': timedelta(minutes=5)
        # 'queue': 'bash_queue',
        # 'pool': 'backfill',
        # 'priority_weight': 10,
        # 'end_date': datetime(2016, 1, 1),
        # 'wait_for_downstream': False,
        # 'sla': timedelta(hours=2),
        # 'execution_timeout': timedelta(seconds=300),
        # 'on_failure_callback': some_function,
        # 'on_success_callback': some_other_function,
        # 'on_retry_callback': another_function,
        # 'sla_miss_callback': yet_another_function,
        # 'trigger_rule': 'all_success'
    },
    description='triggers NubankAPI app run.py',
    schedule_interval='* * * * *',
    start_date=datetime(2021, 1, 1),
    catchup=False,
    tags=['example'],
) as dag:

    run_dbt_agg_models = DbtRunOperator(
        task_id="nubank_dbt_run_daily",
        project_dir="s3://dbt-backend/nubank_transactions/",
        profiles_dir="s3://dbt-backend/",
        # select=["+tag:daily"],
        # exclude=["tag:deprecated"],
        target="dev",
        profile="quick_start",
   )
    success = BashOperator(
        task_id='print_success',
        bash_command='echo "Success"'
    )

    run_dbt_agg_models >> success